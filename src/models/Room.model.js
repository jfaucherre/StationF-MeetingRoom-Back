const mongoose = require('mongoose')

const RoomSchema = mongoose.Schema({
  name: {type: String, required: true, unique: true},
  description: {type: String},
  capacity: {type: Number},
  equipements: [{type: String}],
}, {
  usePushEach: true,
  timestamps: true,
})

RoomSchema.virtual('serialize').get(function() {
  return {
    id: this._id,
    description: this.description,
    capacity: this.capacity,
    equipements: this.equipements,
  }
})

const Room = mongoose.model('Room', RoomSchema)

module.exports = Room
