const mongoose = require('mongoose')

const ReservationSchema = mongoose.Schema({
  roomId: {type: mongoose.Schema.Types.ObjectId, ref: 'Room', required: true},
  start: {type: Date, required: true},
  end: {type: Date, required: true},
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
}, {
  usePushEach: true,
  timestamps: true,
})

ReservationSchema.virtual('serialize').get(function() {
  return {
    id: this._id,
    roomId: this.roomId,
    start: this.start,
    end: this.end,
    user: this.user,
  }
})

const Reservation = mongoose.model('Reservation', ReservationSchema)

module.exports = Reservation
