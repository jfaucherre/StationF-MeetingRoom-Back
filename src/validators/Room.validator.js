const mongoose = require('mongoose')

const errorHandler = require('../utils/errorHandler.js').errorHandler
const CredentialError = require('../utils/errorHandler.js').CredentialError
const ValidationError = require('../utils/errorHandler.js').ValidationError

module.exports = class RoomValidator {

  static createRoom (req, res, next) {
    return errorHandler(new CredentialError(), res)
  }

  static getRoomByCapacityAndEquipements (req, res, next) {
    const capacity = req.params.capacity

    if (!capacity || capacity < 2)
      return errorHandler(new ValidationError('capacity', 'invalid'))

    return next()
  }

}
