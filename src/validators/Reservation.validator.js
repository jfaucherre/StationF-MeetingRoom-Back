const mongoose = require('mongoose')
const errorHandler = require('../utils/errorHandler.js').errorHandler
const CredentialError = require('../utils/errorHandler.js').CredentialError
const ValidationError = require('../utils/errorHandler.js').ValidationError
const verifyDates = require('../utils/verifyDates.js')

module.exports = class ReservationValidator {

  static createReservation (req, res, next) {

    if (!mongoose.Types.ObjectId.isValid(req.body.roomId)) {
      return errorHandler(new ValidationError('roomId', 'invalid'), res)
    }

    const error = verifyDates(req, 'body')

    if (error) {
      errorHandler(error, res)
    }

    return next()

  }

  static deleteReservationById (req, res, next) {

  if (!mongoose.Types.ObjectId.isValid(req.params.reservationId)) {
    return errorHandler(new ValidationError('reservationId', 'invalid'), res)
  }

  return next()

  }

  static getPreciseRoomReservations (req, res, next) {

    if (!mongoose.Types.ObjectId.isValid(req.params.roomId)) {
      return errorHandler(new ValidationError('roomId', 'invalid'), res)
    }

    const error = verifyDates(req, 'params')

    if (error) {
      errorHandler(error, res)
    }

    return next()

  }

  static deleteAllReservations (req, res, next) {
    //return errorHandler(new CredentialError, res)
    return next()
  }

}
