const DateError = require('./errorHandler.js').DateError

const minHours = 8
const maxHours = 18
const diffDays = 7
const maxDuration = 2

const roundTime = (time) => {
  if (!time)
    return time
  if (time.getMinutes() > 52)
    time.setHours(time.getHours() + 1)
  time.setMinutes((Math.round(time.getMinutes() / 15) * 15) % 60)
  return time
}

module.exports = (req, where) => {
  const start = roundTime(new Date(req[where].start))
  const end = roundTime(new Date(req[where].end))

  if (isNaN(start.getTime()) || isNaN(end.getTime())) {
    return new DateError('dates must be ISO formatted')
  }

  const first = new Date()
  const last = new Date()
  first.setHours(first.getHours() + 1)
  last.setDate(last.getDate() + diffDays)
  last.setHours(maxHours)
  last.setMinutes(0)
  last.setSeconds(0)

  const diff = end - start

  if (start >= end) {
    return new DateError('the beginning is after or on the end')
  } else if (diff > maxDuration * 60 * 60 * 1000) {
    return new DateError('the duration is too long')
  } else if (start < first) {
    return new DateError('the beginning is too early')
  } else if (end > last) {
    return new DateError('the end is too late')
  } else {
    req[where].start = start
    req[where].end = end
    return null
  }
}
