module.exports = (dst, src) => {
  const res = []
  for (elem of dst) {
    if (src.indexOf(elem) !== -1) {
      res.push(elem)
    }
  }
  return res
}
