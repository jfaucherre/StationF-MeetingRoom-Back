class notFoundError {
  constructor (target) {
    this.target = target
  }
}

class ValidationError {
  constructor (target, action) {
    this.target = target
    this.action = action
  }
}

class CredentialError {
}

class DateError {
  constructor (reason) {
    this.reason = reason
  }
}

const errorHandler = (err, res, message) => {
  if (err instanceof notFoundError) {
    return res.status(404).json({ results: null, message: `${err.target} not found` })
  } else if (err instanceof ValidationError) {
    return res.status(400).json({ results: null, message: `Parameter ${err.target} is ${err.action}` })
  } else if (err instanceof CredentialError) {
    return res.status(403).json({ results: null, message: 'You are not allowed to perform this action'})
  } else if (err instanceof DateError) {
    return res.status(400).json({ results: null, message: `Could not create reservation because ${err.reason}` })
  }
  res.status(500).json({ results: null, message })
  console.log(err)
}

module.exports = {
  errorHandler,
  CredentialError,
  DateError,
  ValidationError
}
