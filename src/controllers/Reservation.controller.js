const filter = require('filter-object')
const errorHandler = require('../utils/errorHandler.js').errorHandler
const DateError = require('../utils/errorHandler.js').DateError
const notFoundError = require('../utils/errorHandler.js').notFoundError

const permitted = '{roomId,start,end}'

module.exports = class ReservationController {

  static getAllReservations (req, res) {

    Reservation.find({})
      .then(reservations => {

        if (!reservations || !reservations.length) { return res.json({ results: [], message: 'No Reservations' }) }

        res.json({
          results: reservations.map(r => r.serialize),
          message: 'Reservations successfully found',
        })

      })
      .catch(err => errorHandler(err, res, 'Error while getting reservations'))
  }

  static createReservation (req, res) {

    const reservationParams = filter(req.body, permitted)

    return Reservation.find({
      roomId: reservationParams.roomId,
      start: {$lt: reservationParams.end},
      end: {$gt: reservationParams.start},
    })
    .then(reservations => {
      if (reservations && reservations.length != 0) { return Promise.reject(new DateError('this timing was already taken')) }
      return new Reservation(reservationParams)
      .save()
    })
    .then(savedReservations => res.status(201).json({ results: savedReservations.serialize, message: 'Reservation successfully created'}))
    .catch(err => errorHandler(err, res, 'Error while creating reservation'))

  }

  static getPreciseRoomReservations (req, res) {

    const reservationParams = filter(req.params, permitted)

    Reservation.find({
      roomId: reservationParams.roomId,
      start: {$lt: reservationParams.end},
      end: {$gt: reservationParams.start},
    })

    .then(reservations => {

      if (!reservations || !reservations.length) { return res.json({ results: [], message: 'No Reservations' }) }

      res.json({
        results: reservations.map(r => r.serialize),
        message: 'Reservations successfully found',
      })

    })
    .catch(err => errorHandler(err, res, 'Error while getting reservations'))

  }

  static deleteReservationById (req, res) {

    Reservation.remove({ _id: req.params.reservationId })

      .then(del => {
        if (!del.result.n) { return Promise.reject(notFoundError('Reservation')) }
        res.json({ results: null, message: 'Reservation successfully deleted' })
      })

      .catch(err => errorHandler(err, res, 'Error while deleting Reservation'))

  }

  static deleteAllReservations (req, res) {
    Reservation.remove({})

      .then(del => {
        if (!del.result.n) { return Promise.reject(notFoundError('Reservation')) }
        res.json({ results: null, message: 'Reservation successfully deleted' })
      })

      .catch(err => errorHandler(err, res, 'Error while deleting Reservation'))

  }
}
