const filter = require('filter-object')
const errorHandler = require('../utils/errorHandler.js').errorHandler
const arrayContains = require('../utils/arrayContains.js')

const permitted = '{name,description,capacity,equipements}'
const permittedEquiments = ['TV', 'Retro Projecteur']

module.exports = class RoomController {
  static createRoom (req, res) {
    new Room(filter(req.body, permitted))
      .save()
      .then(savedRoom => res.status(201).json({ results: savedRoom.serialize, message: 'Room successfully created' }))
      .catch(err => errorHandler(err, res, 'Error while creating Room'))
  }

  static getAllRooms (req, res) {
    Room.find({})
      .then(rooms => {
        if (!rooms || !rooms.length) { return res.json({ results: [], message: 'No Rooms'}) }
        res.json({
          results: rooms.map(r => r.serialize),
          message: 'Rooms successfully found',
        })
      })
      .catch(err => errorHandler(err, res, 'Error while getting Rooms'))
  }

  static getRoomByCapacityAndEquipements (req, res) {
    const query = {
      capacity: {$gte: req.params.capacity},
    }

    const reqEquipements = arrayContains(JSON.parse(req.params.equipements), permittedEquiments)

    if (reqEquipements.length) {
      query.equipements = {$all: reqEquipements}
    }

    Room.find(query)

      .then(rooms => {

        if (!rooms || !rooms.length) { return res.json({ results: [], message: 'No Rooms'}) }

        res.json({
          results: rooms.map(r => r.serialize),
          message: 'Rooms successfully found',
        })

      })
      .catch(err => errorHandler(err, res, 'Error while getting Rooms'))
  }
}
