const cors = require('cors')
const express = require('express')
const mongoose = require('mongoose')
const bodyParser = require('body-parser')

const configs = require('../config/')
const createRouter = require('./routes/')

// const Logger = require('./utils/Logger.js')
const Logger = console.log

const app = express()

const env = process.env.node_end || 'development'
const config = configs[env]

global.Reservation = require('./models/Reservation.model.js')
global.Room = require('./models/Room.model.js')

app.use(cors())

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

mongoose.Promise = global.Promise

mongoose.connect(`mongodb://${config.db.host}:${config.db.port}/${config.db.dbName}`)
const db = mongoose.connection
db.on('error', err => {
  Logger('FAILED TO CONNECT', err)
  process.exit(1)
})

db.once('open', () => {
  createRouter(app)
  app.listen(config.server.port)
  app.emit('ready')
  Logger(`App is running and listening to port ${config.server.port}`)
})
