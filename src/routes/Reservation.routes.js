const reservationController = require('../controllers/Reservation.controller.js')
const reservationValidator = require('../validators/Reservation.validator.js')

module.exports = [
  {
    method: 'GET',
    path: '/reservations',
    validators: [],
    handler: reservationController.getAllReservations,
  },
  {
    method: 'POST',
    path: '/reservations',
    validators: [reservationValidator.createReservation],
    handler: reservationController.createReservation,
  },
  {
    method: 'GET',
    path: '/rooms/:roomId/reservations/:start/:end',
    validators: [reservationValidator.getPreciseRoomReservations],
    handler: reservationController.getPreciseRoomReservations,
  },
  {
    method: 'DELETE',
    path: '/reservation/:reservationId',
    validators: [reservationValidator.deleteReservationById],
    handler: reservationController.deleteReservationById,
  },
  {
    method: 'DELETE',
    path: '/reservations',
    validators: [reservationController.deleteAllReservations],
    handler: reservationController.deleteAllReservations,
  },
]
