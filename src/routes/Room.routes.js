const roomController = require('../controllers/Room.controller.js')
const roomValidator = require('../validators/Room.validator.js')

module.exports = [
  {
    method: 'GET',
    path: '/rooms',
    validators: [],
    handler: roomController.getAllRooms,
  },
  // This route is usually disabled you have to had it manually... For the moment, ie until there is a jwt in place
  {
    method: 'POST',
    path: '/rooms',
    validators: [roomValidator.createRoom],
    handler: roomController.createRoom,
  },
  {
    method: 'GET',
    path: '/rooms/capacity/:capacity/equipements/:equipements',
    validators: [roomValidator.getRoomByCapacityAndEquipements],
    handler: roomController.getRoomByCapacityAndEquipements,
  },
]
