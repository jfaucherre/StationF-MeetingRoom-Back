const express = require('express')

const reservationRoutes = require('./Reservation.routes.js')
const roomRoutes = require('./Room.routes.js')

const routes = [
  ...reservationRoutes,
  ...roomRoutes,
]

module.exports = (app) => {
  const router = express.Router()

  routes.forEach(r => {
    if (r.validators) {
      router[r.method.toLowerCase()](r.path, ...r.validators, r.handler)
    } else {
      router[r.method.toLowerCase()](r.path, r.handler)
    }
  })
  app.use(router)
}
