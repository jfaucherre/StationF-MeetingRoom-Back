module.exports = {
  db: {
    host: 'localhost',
    port: '27017',
    dbName: 'meeting-room-reservation',
  },
  server: {
    port: '8080',
    url: 'localhost:8080',
  },
}

